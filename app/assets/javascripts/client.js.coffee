ready = ->

	$('#client_cep').focusout ->
		cep = $('#client_cep').val().replace(/\D/g, "")
		cep = if cep.length > 0 then cep else '00000000'
		$.ajax
			url: '/address/' + cep
			dataType: 'json'
			success: (data) ->
				if jQuery.isEmptyObject data.address
					$('#client_address').val("")
					$('#client_city').val("")
					$('#client_state').val("")
					$('#client_complement').val("")
				else
					$('#client_address').val(data.address.address + ', ' + data.address.neighborhood)
					$('#client_city').val(data.address.city)
					$('#client_state').val(data.address.state)
					$('#client_complement').val(data.address.complement)
				return
			error: (data) ->
				$('#client_address').val("")
				$('#client_city').val("")
				$('#client_state').val("")
				$('#client_complement').val("")
				return
	return

$(document).ready(ready)
$(document).on('page:load', ready)
