ready = ->

  $('.datetimepicker').datepicker
    format: 'dd/mm/yyyy'
    language: 'pt-BR'

  # Scheduling page
  modelsScheduling = $('#scheduling_client_id').html()
  $('#scheduling_parent_id').change ->
    parent = $('#scheduling_parent_id :selected').text()
    escaped_parent = parent.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(modelsScheduling).filter("optgroup[label='#{escaped_parent}']").html()
    if options
      $('#scheduling_client_id').html(options)
    else
      $('#scheduling_client_id').empty('<option value=""></option>')

  $("#scheduling_parent_id").trigger "change"

  # Report page
  models = $('#client').html()
  $('#parent').change ->
    parent = $('#parent :selected').text()
    escaped_parent = parent.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(models).filter("optgroup[label='#{escaped_parent}']").html()
    if options
      $('#client').html(options)
      $('#client').prepend('<option value=""></option>')
    else
      $('#client').empty('<option value=""></option>')

  $("#parent").trigger "change"

  elems = Array::slice.call(document.querySelectorAll('.js-switch'))
  elems.forEach (html) ->
    switchery = new Switchery(html, color: '#1AB394')
    return

  return

$(document).ready(ready)
$(document).on('page:load', ready)
