$(document).ready ->
  $('.datatable').DataTable
    paging: false
    info: false
    language: 'url': '/pt-br.lang'
    responsive: true
    order: []
    columnDefs: [
      {
        responsivePriority: 1
        targets: 0
      }
      {
        responsivePriority: 2
        targets: -1
      }
    ]
  return
