class AccountActivationsController < ApplicationController
  skip_after_action :verify_authorized
  layout 'session'

  def edit
    @user = User.find_by(email: params[:email])
    if @user && !@user.activated? && @user.authenticated?(:activation, params[:id])
      render :edit
    else
      flash[:danger] = "Link de ativação inválido."
      redirect_to login_url
    end
  end

  def update
    @user = User.find_by(email: params[:email])
    respond_to do |format|
      if @user.update(user_params)
        @user.activate
        log_in @user
        format.html { redirect_to root_url, flash: { success: "Conta ativada." } }
        format.json { head :no_content }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def request_resend
    @user = User.new
    render :resend
  end

  def resend
    @user = User.find_by(email: user_params[:email])
    if @user && !@user.activated?
      @user.resend_activation_email
      flash[:success] = "Link de ativação reenviado."
      redirect_to login_url
    else
      flash[:danger] = "Conta já ativada."
      redirect_to login_url
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation, :email)
  end
end
