class AddressController < ApplicationController
  after_action :skip_authorization
  def show
    @address = Correios::CEP::AddressFinder.get(params[:cep]) rescue {}
    render :json => { address: @address }
  end
end
