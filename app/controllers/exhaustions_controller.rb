# encoding: UTF-8
class ExhaustionsController < ApplicationController
  before_action :set_exhaustion, only: [:show, :edit, :update, :destroy]

  # GET /exhaustions/new
  def new
    @exhaustion = Exhaustion.new(scheduling_id: params[:scheduling_id])
    authorize @exhaustion
  end

  # GET /exhaustions/1
  def show

    respond_to do |format|
      format.html
      # format.pdf do
      #
      # end
    end
  end

  def pdf_create
      @exhaustion = Exhaustion.find(params[:exhaustion_id])
      authorize @exhaustion

      pdf = ExhaustionPdf.new(@exhaustion, params[:enterprise], view_context)
      send_data pdf.render,
      filename: "exaustao_#{@exhaustion.id}.pdf",
      type: "application/pdf",
      disposition: "inline"
  end

  # POST /exhaustions
  # POST /exhaustions.json
  def create
    @exhaustion = Exhaustion.new(exhaustion_params)
    authorize @exhaustion
    respond_to do |format|
     if @exhaustion.save
      format.html { redirect_to @exhaustion, notice: 'Exaustão realizada com sucesso.' }
      format.json { render json: @exhaustion, status: :created, location: @exhaustion }
    else
      format.html { render action: "new" }
      format.json { render json: @exhaustion.errors, status: :unprocessable_entity }
    end
  end
end

  # GET /exhaustions/1/edit
  def edit
  end

  # PATCH/PUT /exhaustions/1
  # PATCH/PUT /exhaustions/1.json
  def update
    respond_to do |format|
      if @exhaustion.update(exhaustion_params)
        format.html { redirect_to @exhaustion, notice: 'Exaustão atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @exhaustion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /exhaustions/1
  # DELETE /exhaustions/1.json
  def destroy
    @exhaustion.destroy
    respond_to do |format|
      format.html { redirect_to schedulings_path, notice: 'Exaustão removida com sucesso.' }
      format.json { head :no_content }
    end
  end

  # GET /exhaustions/1/pictures
  def pictures
     @exhaustion = Exhaustion.find(params[:exhaustion_id])
     authorize @exhaustion
  end

  # POST /exhaustions/1/pictures
  def upload
    exhaustion = Exhaustion.find(params[:exhaustion_id])
    authorize exhaustion
    @picture = Picture.new(picture_params)
    @picture.execution = exhaustion.execution
    @picture.image = params[:file]
    @picture.flag = params[:flag]
    @picture.save!
    render json: @picture
  end

   # DELETE /exhaustions/1/picture
  def destroy_image
    @exhaustion = Exhaustion.find(params[:exhaustion_id])
    authorize @exhaustion
    @picture = Picture.find(params[:picture_id])
    @picture.destroy
    respond_to do |format|
      format.html { redirect_to @exhaustion, notice: 'Imagem removida com sucesso.' }
      format.json { head :no_content }
    end
  end

  private

  def set_exhaustion
    @exhaustion = Exhaustion.find(params[:id])
    authorize @exhaustion
  end

  def exhaustion_params
    params.require(:exhaustion).permit(:manager, :scheduling_id, :observation)
  end

  def picture_params
    params.require(:picture).permit(:exhaustion_id, :picture, :flag)
  end
end
