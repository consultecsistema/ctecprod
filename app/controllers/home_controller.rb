class HomeController < ApplicationController
	skip_after_action :verify_authorized, only: [:error]
	def index
    authorize :page, :view?
		@mainTitle = "Olá!"
		@mainDesc = "Vamos iniciar, acesse sua dashboard utilizando o menu superior."
	end

	def error
		render :file => 'public/500.html', :status => :not_found, :layout => false
	end
end
