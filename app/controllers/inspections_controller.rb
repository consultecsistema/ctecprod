# encoding: UTF-8
class InspectionsController < ApplicationController
  before_action :set_inspection, only: [:show, :edit, :update, :destroy]

  # GET /inspections/new
	def new
		@inspection = Inspection.new(scheduling_id: params[:scheduling_id])
    authorize @inspection
	end

  # GET /inspections/1
	def show
    respond_to do |format|
      format.html
      format.pdf do
        pdf = InspectionPdf.new(@inspection, view_context)
          send_data pdf.render,
          filename: "vistoria_#{@inspection.id}.pdf",
          type: "application/pdf",
          disposition: "inline"
      end
    end
	end

  # POST /inspections
  # POST /inspections.json
	def create
		@inspection = Inspection.new(inspection_params)
    authorize @inspection
		respond_to do |format|
			if @inspection.save
				format.html { redirect_to @inspection, notice: 'Vistoria realizada com sucesso.' }
				format.json { render json: @inspection, status: :created, location: @inspection }
			else
				format.html { render action: "new" }
				format.json { render json: @inspection.errors, status: :unprocessable_entity }
			end
		end
	end

  # GET /inspections/1/edit
  def edit
  end

  # PATCH/PUT /inspections/1
  # PATCH/PUT /inspections/1.json
  def update
    respond_to do |format|
      if @inspection.update(inspection_params)
        format.html { redirect_to @inspection, notice: 'Vistoria atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @inspection.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inspections/1
  # DELETE /inspections/1.json
  def destroy
    @inspection.destroy
    respond_to do |format|
      format.html { redirect_to schedulings_path, notice: 'Vistoria removida com sucesso.' }
      format.json { head :no_content }
    end
  end

  # GET /inspections/1/pictures
  def pictures
     @inspection = Inspection.find(params[:inspection_id])
     authorize @inspection
  end

  # POST /inspections/1/pictures
  def upload
    inspection = Inspection.find(params[:inspection_id])
    authorize inspection
    @picture = Picture.new(picture_params)
    @picture.execution = inspection.execution
    @picture.image = params[:file]
    @picture.flag = params[:flag]
    @picture.save!
    render json: @picture
  end

   # DELETE /inspections/1/picture
  def destroy_image
    @inspection = Inspection.find(params[:inspection_id])
    authorize @inspection
    @picture = Picture.find(params[:picture_id])
    @picture.destroy
    respond_to do |format|
      format.html { redirect_to @inspection, notice: 'Imagem removida com sucesso.' }
      format.json { head :no_content }
    end
  end

private

  def set_inspection
    @inspection = Inspection.find(params[:id])
    authorize @inspection
  end

	def inspection_params
		params.require(:inspection).permit(:manager, :scheduling_id,
			:hoods_state, :hoods_clean, :flame_trap, :flame_damper, :flame_system,
			:fog, :risk, :exhaustion, :window, :ducts, :observation)
	end

  def picture_params
    params.require(:picture).permit(:inspection_id, :picture, :flag)
  end
end
