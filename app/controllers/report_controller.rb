class ReportController < ApplicationController
  def show
    authorize :page, :view?
  end

  def generate
    authorize :page, :view?
    if !params[:service].blank? &&
       (!params[:parent].blank? || !params[:client].blank?) &&
       !params[:start].blank? &&
       !params[:end].blank?
      pdf = ManagementPdf.new(params, view_context)
      send_data pdf.render,
                filename: 'gerencial.pdf',
                type: 'application/pdf',
                disposition: 'inline'
    else
      flash.now[:danger] = 'Preencha as todas opções de filtro.'
      render :show
    end
  end
end
