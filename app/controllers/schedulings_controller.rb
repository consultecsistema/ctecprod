class SchedulingsController < ApplicationController
  before_action :set_scheduling, only: [:show, :edit, :update, :destroy]
  require 'date'
  # GET /schedulings
  # GET /schedulings.json
  def index
    authorize Scheduling

    if params[:scheduling].nil?
      @schedulings = policy_scope(Scheduling).paginate(:page => params[:page], :per_page => 100)
    else
      search = params[:scheduling][:search] if !params[:scheduling][:search].nil?


      case params[:scheduling][:type]
      when 'client'
        clients_id = Client.where("name LIKE UPPER(?) ", "%#{search.upcase}%").pluck(:id)
        @schedulings = policy_scope(Scheduling).where(client_id: clients_id).paginate(:page => params[:page], :per_page => 100)
      when 'filial'
        filials = Client.where("name LIKE UPPER(?) ", "%#{search.upcase}%").pluck(:id)
        @schedulings = policy_scope(Scheduling).where(client_id: clients_id).paginate(:page => params[:page], :per_page => 100)
      when 'data'
        search = DateTime.parse(search)
        search = search.strftime('%Y-%m-%d')

        @schedulings = policy_scope(Scheduling.where("DATE(date) = ?","#{search}")).paginate(:page => params[:page], :per_page => 100)
      end


    end
  end

  # GET /schedulings/new
  def new
    @scheduling = Scheduling.new
    authorize @scheduling
  end

  # GET /schedulings/1/edit
  def edit
  end

  # POST /schedulings
  # POST /schedulings.json
  def create
    @scheduling = Scheduling.new(scheduling_params)
    authorize @scheduling
    respond_to do |format|
      if @scheduling.save
        format.html { redirect_to schedulings_url, notice: 'Agendamento criado com sucesso.' }
        format.json { render :show, status: :created, location: @scheduling }
      else
        format.html { render :new }
        format.json { render json: @scheduling.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schedulings/1
  # PATCH/PUT /schedulings/1.json
  def update
    respond_to do |format|
      if @scheduling.update(scheduling_params)
        format.html { redirect_to schedulings_url, notice: 'Agendamento atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @scheduling }
      else
        format.html { render :edit }
        format.json { render json: @scheduling.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schedulings/1
  # DELETE /schedulings/1.json
  def destroy
    @scheduling.destroy
    respond_to do |format|
      format.html { redirect_to schedulings_url, notice: 'Agendamento removido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_scheduling
      @scheduling = Scheduling.find(params[:id])
      authorize @scheduling
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def scheduling_params
      params.require(:scheduling).permit(:service, :date, :parent_id, :client_id, :user_id, :search)
    end
end
