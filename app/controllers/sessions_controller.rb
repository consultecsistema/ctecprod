class SessionsController < ApplicationController
  skip_after_action :verify_authorized
  layout 'session'

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        redirect_to root_url
      else
        message  = "Conta não ativada. "
        message += "Verifique seu email com link de ativação."
        flash[:warning] = message
        redirect_to login_url
      end
    else
      flash.now[:danger] = 'Combinação email/senha inválida'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to login_url
  end
end
