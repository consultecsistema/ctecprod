class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    authorize User
    @users = policy_scope(User).where(deleted_at: nil).paginate(:page => params[:page], :per_page => 50)
  end

  # GET /users/new
  def new
    @user = User.new
    authorize @user
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    authorize @user
    respond_to do |format|
      if @user.save
        @user.send_activation_email
        format.html { redirect_to users_url, notice: 'Usuário criado com sucesso. Email de ativação enviado.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to root_path, notice: 'Usuário editado com sucesso.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.deleted_at = Time.now
    count_user = User.where.not(deleted_at: nil).count + 1
    @user.email = "d3l3t4d0LSM#{count_user}@ctecprod.com"
    respond_to do |format|
      if @user.save
        format.html { redirect_to users_url, notice: 'Usuário removido com sucesso.' }
        format.json { head :no_content }
      else
        format.html { redirect_to users_url, notice: 'Usuário não foi removido.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
      authorize @user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :phone, :role, :password, :password_confirmation)
    end
end
