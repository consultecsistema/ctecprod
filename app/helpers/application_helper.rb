module ApplicationHelper
	def is_current_page?(*pages)
		css = nil
		pages.each do |page|
			css = "active" if current_page?(page)
		end
		return css
	end
end
