class ApplicationMailer < ActionMailer::Base
  default from: "me@ctec.herokuapp.com"
  layout 'mailer'
end
