class Client < ActiveRecord::Base
	default_scope { order('name') }
	# belongs_to :parent, class_name: 'Client', foreign_key: :parent_id
	has_many :child, class_name: 'Client', foreign_key: :parent_id
	mount_uploader :image, ImageUploader
	validates :name, presence: true

  def full_address
    "#{address}, #{number}, #{state} - #{city} #{cep}"
  end

	def label
    "#{name} - #{cnpj}"
  end
end
