class Execution < ActiveRecord::Base
	actable
	belongs_to :scheduling
	has_many :pictures, dependent: :destroy
	validates :manager, :scheduling, presence: true
end
