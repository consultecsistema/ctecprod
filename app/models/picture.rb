class Picture < ActiveRecord::Base
  belongs_to :execution
  enum flag: [:before, :after]
  mount_uploader :image, ImageUploader
  validates :image, file_size: { less_than_or_equal_to: 2.megabytes }
  default_scope { order('id') }
end
