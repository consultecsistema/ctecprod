class Scheduling < ActiveRecord::Base
	enum service: [ :vistoria, :limpeza_exaustao, :limpeza_ar_condicionado ]
  belongs_to :client
  belongs_to :parent, class_name: 'Client', foreign_key: :parent_id
  belongs_to :user
  has_one :execution, dependent: :destroy
  validates :service, :parent, :client, :date, :user, presence: true
	default_scope { order date: :desc }
end
