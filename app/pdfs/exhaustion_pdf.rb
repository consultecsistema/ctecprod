require "open-uri"

class ExhaustionPdf < Prawn::Document
  def initialize(exhaustion, enterprise, view)
    super(top_margin: 70)
    @exhaustion = exhaustion
    @credentials = credentials_validate enterprise
    @view = view
    define_grid(:columns => 8, :rows => 10, :gutter => 10)
    first_page
    start_new_page
    second_page
    start_new_page
    third_page
    number_pages "<page>", at: [500,-10]
  end

  def first_page
    grid([0, 0], [0, 7]).bounding_box do
      image @credentials['logo_url'], :scale => 0.7, :at => [0, 115]
    end

    grid([0, 3], [0, 7]).bounding_box do
      text @credentials['enterprise_name']
    end

    grid([4, 2], [8, 5]).bounding_box do
      text "Realizado em #{@exhaustion.scheduling.date}", align: :center, style: :bold, size: 16, color: 'FF0000'
      text @exhaustion.scheduling.parent.name, align: :center, style: :bold, size: 16
      text @exhaustion.scheduling.client.name, align: :center, style: :bold, size: 16
      image open(@exhaustion.scheduling.client.image.url), position: :center, width: 200, height: 200 unless @exhaustion.scheduling.client.image.blank?
    end
  end

  def second_page
    text "1 - Introducão", style: :bold, size: 18
    move_down 10
    infos = "Relatório dos serviços de manutenção realizados no #{@exhaustion.scheduling.client.name} no dia #{@exhaustion.scheduling.date}"
    infos += ", #{@exhaustion.scheduling.client.full_address}" unless @exhaustion.scheduling.client.cep.blank?
    infos += "."
    infos += " Gerente de atendimento: #{@exhaustion.manager}"
    text infos
    move_down 20
    text "Fotos Antes | Depois:", style: :bold
    pictures(@exhaustion.pictures.before, @exhaustion.pictures.after)
  end

  def pictures(before, after)

      move_down 20
      images = []
      unless before.blank?
        before.each_with_index do |before, index|
          pic = []
          pic << { image: open(before.image.url), position: :center, fit: [200,200]} if before
          pic << { image: open(after[index].image.url), position: :center, fit: [200,200]} if after[index]
          images << pic
        end
        table images, :width => bounds.width
      end

  end

  def third_page
    text "2 - Conclusão", style: :bold, size: 18
    move_down 10

    if @exhaustion.scheduling.service == 'limpeza_exaustao'
      conclusion = " Os serviços de limpeza foram realizados em conformidades com os padrões NBR - 14518."
    else
      conclusion = " Os serviços de limpeza foram realizados em conformidades com os padrões NBR - 14679"
    end
    text conclusion
    move_down 10
    if @exhaustion.observation
      text "Observação:", style: :bold
      move_down 10
      text @exhaustion.observation
    end
    move_down 50
    text "comercial@consulltec.com.br", align: :center
    move_down 10
    image @credentials['assign_url'], position: :center
    stroke_horizontal_rule
    pad_top(5) { text @credentials['name'], align: :center }
    move_down 5
    text @credentials['crea'], align: :center, size: 10
    text @credentials['phones'], align: :center, size: 10

  end

  def credentials_validate enterprise
    credentials = {}
    case enterprise
    when 'consulltec'
      credentials['name'] = 'NATANAILSON ALVES DOS SANTOS'
      credentials['crea'] = 'CONSULTOR DE MANUTENÇÃO - CREA - PE N. 056615'
      credentials['phones'] = '(81) 9726-1676/(81) 3033-2215'
      credentials['assign_url'] = "#{Rails.root}/app/assets/images/consulltec_sign.png"
      credentials['logo_url'] = "#{Rails.root}/app/assets/images/consulltec_logo.png"
      credentials['enterprise_name'] = 'CONSULLTEC – CONSULTORIA E ASSISTÊNCIA TÉCNICA'
    when 'mn'
      credentials['name'] = "MARCOS VENICIUS EUBANQUE DELAZARI"
      credentials['crea'] = 'ENGENHEIRO MECÂNICO - CREA - SP N. 5060331361'
      credentials['phones'] = '(11)  2378-9302'
      credentials['assign_url'] = "#{Rails.root}/app/assets/images/rclear_marco_venicius.jpg"
      credentials['logo_url'] = "#{Rails.root}/app/assets/images/mn.jpg"
      credentials['enterprise_name'] = 'MN- INSTALAÇÕES E REPARAÇÕES INDUSTRIAL LTDA'
    when 'rb_marco_antonio'
      credentials['name'] = 'MARCO ANTONIO PORTO BERNSTS'
      credentials['crea'] = 'ENGENHEIRO MECÂNICO & SEGURANÇA DO TRABALHO - CREA - SP N. 5069655724'
      credentials['phones'] = '(11) 99895-4503'
      credentials['assign_url'] = "#{Rails.root}/app/assets/images/rclear_marco_antonio.png"
      credentials['logo_url'] = "#{Rails.root}/app/assets/images/roboclear_logo.jpg"
      credentials['enterprise_name'] = " ROBOCLEAR LIMPEZA PROFISSIONAL DE SISTMA DE EXAUSTÃO E QUALIDADE DO AR EM AR CONDICIONADO E LTDA"
    when 'rb_marco_venicius'
      credentials['name'] = 'MARCOS VENICIUS EUBANQUE DELAZARI'
      credentials['crea'] = 'ENGENHEIRO MECÂNICO - CREA - SP N. 5060331361'
      credentials['phones'] = '(11) 99895-4503'
      credentials['assign_url'] = "#{Rails.root}/app/assets/images/rclear_marco_venicius.jpg"
      credentials['logo_url'] = "#{Rails.root}/app/assets/images/roboclear_logo.jpg"
      credentials['enterprise_name'] = " ROBOCLEAR LIMPEZA PROFISSIONAL DE SISTMA DE EXAUSTÃO E QUALIDADE DO AR EM AR CONDICIONADO E LTDA"
    end
    credentials
  end
end
