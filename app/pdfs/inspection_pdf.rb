require "open-uri"

class InspectionPdf < Prawn::Document
  def initialize(inspection, view)
    super(top_margin: 70)
    @inspection = inspection
    @view = view
    define_grid(:columns => 8, :rows => 10, :gutter => 10)
    first_page
    start_new_page
    second_page
  end

  def first_page
    grid([0, 0], [0, 7]).bounding_box do
      image "#{Rails.root}/app/assets/images/logo.png", :scale => 0.7, :at => [0, 115]
    end

    grid([0, 3], [0, 7]).bounding_box do
      text 'CONSULLTEC – CONSULTORIA E ASSISTÊNCIA TÉCNICA'
    end

    grid([4, 2], [8, 5]).bounding_box do
      text "RELATÓRIO DE VISTORIA", size: 18, align: :center, style: :bold
      text "Realizado em #{@inspection.scheduling.date}", align: :center, style: :bold, size: 16, color: 'FF0000'
      text @inspection.scheduling.parent.name, align: :center, style: :bold, size: 16
      text @inspection.scheduling.client.name, align: :center, style: :bold, size: 16
      image open(@inspection.scheduling.client.image.url), position: :center, width: 200, height: 200 unless @inspection.scheduling.client.image.blank?
    end
  end

  def second_page
    text "VISTORIA INTERIOR - LOJA", style: :bold, align: :center
    stroke_horizontal_rule

    move_down 30
    text "Loja: #{@inspection.scheduling.client.name}", style: :bold
    text "Data: #{@inspection.scheduling.date}", style: :bold
    text "Gerente de plantão: #{@inspection.manager}", style: :bold
    text "Avaliador: #{@inspection.scheduling.user.name}", style: :bold
    move_down 10
    if @inspection.observation
      text "Observação:", style: :bold
      move_down 10
      text @inspection.observation
    end

    move_down 20
    text "QUESTIONÁRIO", style: :bold, align: :center
    stroke_horizontal_rule
    questions
    move_down 20
    text "Fotos Antes | Depois:", style: :bold unless @inspection.pictures.before.blank? and @inspection.pictures.after.blank?
    pictures(@inspection.pictures.before, @inspection.pictures.after)
    move_down 20
    text "DE ACORDO:", align: :center
    move_down 35
    stroke_horizontal_rule
    pad_top(5) { text "Assinatura cliente", align: :center }
    move_down 20
    image "#{Rails.root}/app/assets/images/sign.png", position: :center
    stroke_horizontal_rule
    pad_top(5) { text "NATANAILSON ALVES DOS SANTOS", align: :center }
    move_down 5
    text "CONSULTOR DE MANUTENÇÃO - CREA - PE N. 056615", align: :center, size: 10
    text "(81) 9726-1676/(81) 3033-2215", align: :center, size: 10
  end

  def pictures(before, after)
    unless before.blank? and after.blank?
      move_down 20
      images = []
      before.zip(after).each do |b, a|
        pic = []
        pic << { image: open(b.image.url), position: :center, fit: [200,200]} if b
        pic << { image: open(a.image.url), position: :center, fit: [200,200]} if a
        images << pic
      end
      table images, :width => bounds.width
    end
  end

  def questions
    move_down 10
    table line_item_rows, cell_style: { size: 8 }, row_colors: ["F7F7F7", "FFFFFF"], position: :center, width: 540 do
      cells.padding = 10
      cells.borders = []
    end
  end

  def line_item_rows
    itens = []
    itens << item(@inspection.hoods_state, :hoods_state)
    itens << item(@inspection.hoods_clean, :hoods_clean)
    itens << item(@inspection.flame_trap, :flame_trap)
    itens << item(@inspection.flame_damper, :flame_damper)
    itens << [
        Inspection.human_attribute_name(:flame_system),
        bool(@inspection.flame_system),
        @inspection.flame_system ? result(:flame_system_yes) : result(:flame_system)
      ]
    itens << item(@inspection.fog, :fog)
    itens << [
        Inspection.human_attribute_name(:risk),
        bool(@inspection.risk),
        @inspection.risk ? result(:risk) : ''
      ]
    itens << item(@inspection.exhaustion, :exhaustion)
    itens << item(@inspection.window, :window)
    itens << item(@inspection.ducts, :ducts)
    itens
  end

  def item(value, key)
    [
      Inspection.human_attribute_name(key),
      bool(value),
      value ?  "" : result(key)
    ]
  end

  def bool(item)
    item ? "Sim" : "Não"
  end

  def result(result)
    Inspection.human_attribute_name("#{result}_result")
  end
end
