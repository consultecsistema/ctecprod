require "open-uri"

class ManagementPdf < Prawn::Document
  def initialize(params, view)
    super(top_margin: 70)
    @view = view
    @start = Date.parse params[:start]
    @end = Date.parse params[:end]
    @service = params[:service] == "0" ? "Inspection" : "Exhaustion"
    @client = Client.find(params[:client]) unless params[:client].empty?
    @parent = Client.find(params[:parent])
    # @schedulings = Scheduling.joins(:client).where(date: (@start.midnight - 1.day)..@end.midnight).group('clients.id, clients.name').order('clients.id, clients.name').select('clients.id, clients.name')
    @groups = Execution.joins(:scheduling => :parent).where(schedulings: { date: (@start.midnight - 1.day)..@end.midnight, parent_id: @parent ? @parent.id : @client.parent.id }).group('clients.id, clients.name').order('clients.id, clients.name').select('clients.id, clients.name')
    define_grid(:columns => 8, :rows => 10, :gutter => 10)
    first_page
    start_new_page
    report
  end

  def first_page
    # stroke_axis
    transparent(0.2) do
      fill_color "5E9CD3"
      fill_ellipse [0, 350], 45, 700
    end
    fill_color "333333"
    grid([0, 1], [0, 7]).bounding_box do
      image "#{Rails.root}/app/assets/images/logo.png", :scale => 0.7, :at => [0, 115]
    end

    # grid([0, 3], [0, 7]).bounding_box do
    #   text 'CONSULLTEC – CONSULTORIA E ASSISTÊNCIA TÉCNICA'
    # end

    grid([4, 2], [8, 5]).bounding_box do
      text "RELATÓRIO GERENCIAL", size: 18, align: :center, style: :bold
      text "Pesquisa entre #{@start} e #{@end}", align: :center, style: :bold, size: 14, color: 'FF0000'
    end

    grid([9, 0], [10, 7]).bounding_box do
      text 'CONSULLTEC – CONSULTORIA E ASSISTÊNCIA TÉCNICA RUA SÃO BOM JESUS DOS PASSOS', size: 9, align: :center
      text 'Nº28; JABOATÃO DOS GUARARAPES - PE CNPJ: 17.299.472/0001-90 TEL: (81) 3033-2215', size: 9, align: :center
    end
  end

  def report
    # stroke_axis
    transparent(0.2) do
      fill_color "5E9CD3"
      fill_ellipse [0, 350], 45, 700
    end
    fill_color "333333"
    @groups.each do |client|
      text client.name, size: 14, align: :center, style: :bold
      move_down 10
      search = @client ?
          { date: (@start.midnight - 1.day)..@end.midnight, parent_id: client.id, client_id: @client.id} :
              { date: (@start.midnight - 1.day)..@end.midnight, parent_id: client.id }
      @executions = Execution.joins(:scheduling => :client).where(schedulings: search, actable_type: @service).order('schedulings.date, clients.id, clients.name')
      infos(@executions)
      move_down 20
    end
  end

  def infos(executions)
    # exhaustions_total = 0
    # inspections_total = 0
    data = [["Serviço", "Data", "Filial", "Técnico"]]
    executions.each do |execution|
      # exhaustions_total += exhaustions = Scheduling.where(date: (@start.midnight - 1.day)..@end.midnight, service: 0, client_id: client.id).count
      # inspections_total += inspections = Scheduling.where(date: (@start.midnight - 1.day)..@end.midnight, service: 1, client_id: client.id).count
      data << [
        execution.scheduling.service.to_s.humanize,
        execution.scheduling.date.to_s,
        execution.scheduling.client.label.to_s,
        execution.scheduling.user.name.to_s
      ]
    end
    # data << ["TOTAL", exhaustions_total, inspections_total]
    table data, cell_style: { size: 8 }, row_colors: ["F7F7F7", "FFFFFF"], position: :center, width: 540, header: true do
      style row(0), :borders => [:bottom], :background_color => "DDDDDD", :font_style => :bold
      cells.padding = 10
      cells.borders = []
    end
  end
end
