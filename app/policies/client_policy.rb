class ClientPolicy < ApplicationPolicy
  def index?  ; user.administrator? or user.moderator?; end
  def create? ; user.administrator? or user.moderator?; end
  def update? ; user.administrator? or user.moderator?; end
  def destroy?; user.administrator?; end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
