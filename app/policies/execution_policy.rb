class ExecutionPolicy < ApplicationPolicy
  def index?          ; true                                                  ; end
  def show?           ; user.administrator? or user.moderator? or record.scheduling.user == user ; end
  def create?         ; user.administrator? or user.moderator? or record.scheduling.user == user ; end
  def update?         ; user.administrator? or user.moderator? or record.scheduling.user == user ; end
  def destroy?        ; user.administrator? or user.moderator?                ; end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
