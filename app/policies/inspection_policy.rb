class InspectionPolicy < ExecutionPolicy
  def pictures?       ; user.administrator? or record.scheduling.user == user ; end
  def upload?         ; pictures?                                             ; end
  def destroy_image?  ; pictures?                                             ; end
end
