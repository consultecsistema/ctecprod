class PagePolicy < Struct.new(:user, :page)
  def initialize(user, page)
    raise Pundit::NotAuthorizedError, "Must be signed in." unless user
    @user = user
    @page = page
  end
  def view?
    true
  end
end
