class SchedulingPolicy < ApplicationPolicy
  def index?  ; true end
  def create? ; user.administrator? or user.moderator?; end
  def update? ; user.administrator? or user.moderator?; end
  def destroy?; user.administrator?; end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if user.administrator? or user.moderator?;
        scope.includes(:client, :user, :parent, execution: [:actable]).all
      else
        scope.includes(:client, :user, :parent, execution: [:actable]).where user: user
      end
    end
  end
end
