json.array!(@clients) do |client|
  json.extract! client, :id, :name, :cnpj, :address, :number, :complement, :city, :state, :cep, :contact, :phone, :email
  json.url client_url(client, format: :json)
end
