json.array!(@schedulings) do |scheduling|
  json.extract! scheduling, :id, :type, :date, :client_id, :user_id
  json.url scheduling_url(scheduling, format: :json)
end
