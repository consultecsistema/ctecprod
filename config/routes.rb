Rails.application.routes.draw do

  # root to: "home#error"

	root to: 'home#index'

	resources :exhaustions, except: [:index] do
		get :pictures
		post :pictures, to: "exhaustions#upload"
    delete :pictures, to: "exhaustions#destroy_image"
		post '/pdf', to: 'exhaustions#pdf_create', as: :pdf_create
	end

	resources :inspections, except: [:index] do
    get :pictures
    post :pictures, to: "inspections#upload"
    delete :pictures, to: "inspections#destroy_image"
  end

	resources :schedulings, except: :show
	resources :clients, except: :show
	resources :users, except: :show
	resources	:account_activations,	only: [:edit, :update]
  resources :password_resets,     only: [:new, :create, :edit, :update]

	get    'report',  to: "report#show"
	post   'report', to: "report#generate"
	get    'resend/activation',  to: "account_activations#request_resend"
	post   'resend/activation', to: "account_activations#resend"
	get 	 'address/:cep',  to: "address#show"
	get    'login', 				to: 'sessions#new'
  post   'login', 				to: 'sessions#create'
  delete 'logout', 				to: 'sessions#destroy'
	post 'schedulings/index', to: 'schedulings#index', as: :search
end
