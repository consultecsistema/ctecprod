class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :cnpj
      t.string :address
      t.string :number
      t.string :complement
      t.string :city
      t.string :state
      t.string :cep
      t.string :contact
      t.string :phone
      t.string :email

      t.timestamps null: false
    end
  end
end
