class CreateSchedulings < ActiveRecord::Migration
  def change
    create_table :schedulings do |t|
      t.integer :service
      t.datetime :date
      t.references :client, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
