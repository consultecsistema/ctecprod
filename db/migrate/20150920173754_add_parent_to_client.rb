class AddParentToClient < ActiveRecord::Migration
  def change
    add_column :clients, :parent_id, :integer
    add_index :clients, :parent_id
  end
end
