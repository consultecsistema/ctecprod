class AddParentToSchedulings < ActiveRecord::Migration
  def change
    add_column :schedulings, :parent_id, :integer
    add_index :schedulings, :parent_id
  end
end
