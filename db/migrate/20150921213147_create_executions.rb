class CreateExecutions < ActiveRecord::Migration
  def change
    create_table :executions do |t|
      t.string :manager
      t.actable
      t.timestamps null: false
    end
  end
end
