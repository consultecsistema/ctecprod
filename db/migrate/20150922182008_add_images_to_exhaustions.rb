class AddImagesToExhaustions < ActiveRecord::Migration
  def change
    add_column :exhaustions, :picture1, :string
    add_column :exhaustions, :picture2, :string
    add_column :exhaustions, :picture3, :string
    add_column :exhaustions, :picture4, :string
    add_column :exhaustions, :picture5, :string
    add_column :exhaustions, :picture6, :string
    add_column :exhaustions, :picture7, :string
    add_column :exhaustions, :picture8, :string
    add_column :exhaustions, :picture9, :string
    add_column :exhaustions, :picture10, :string
  end
end
