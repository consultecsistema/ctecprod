class AddSchedulingToExecutions < ActiveRecord::Migration
  def change
    add_reference :executions, :scheduling, index: true, foreign_key: true
  end
end
