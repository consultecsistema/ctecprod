class RemoveImagesFromExhaustions < ActiveRecord::Migration
  def change
    remove_column :exhaustions, :picture1, :string
    remove_column :exhaustions, :picture2, :string
    remove_column :exhaustions, :picture3, :string
    remove_column :exhaustions, :picture4, :string
    remove_column :exhaustions, :picture5, :string
    remove_column :exhaustions, :picture6, :string
    remove_column :exhaustions, :picture7, :string
    remove_column :exhaustions, :picture8, :string
    remove_column :exhaustions, :picture9, :string
    remove_column :exhaustions, :picture10, :string
  end
end
