class AddAvatarsToExhaustions < ActiveRecord::Migration
  def change
    add_column :exhaustions, :pictures, :json
  end
end
