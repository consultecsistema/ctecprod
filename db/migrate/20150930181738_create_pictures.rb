class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.references :execution, index: true, foreign_key: true
      t.string :image
    end
  end
end
