class RemovePicturesFromExhaustions < ActiveRecord::Migration
  def change
    remove_column :exhaustions, :pictures, :json
  end
end
