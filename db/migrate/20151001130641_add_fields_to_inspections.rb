class AddFieldsToInspections < ActiveRecord::Migration
  def change
  	add_column :inspections, :hoods_state, :boolean
    add_column :inspections, :hoods_clean, :boolean
    add_column :inspections, :flame_trap, :boolean
    add_column :inspections, :flame_damper, :boolean
    add_column :inspections, :flame_system, :boolean
    add_column :inspections, :fog, :boolean
    add_column :inspections, :risk, :boolean
    add_column :inspections, :exhaustion, :boolean
    add_column :inspections, :window, :boolean
    add_column :inspections, :ducts, :boolean
  end
end
