class AddFlagToPicture < ActiveRecord::Migration
  def change
    add_column :pictures, :flag, :integer
  end
end
