class AddObservationToExecution < ActiveRecord::Migration
  def change
    add_column :executions, :observation, :text
  end
end
