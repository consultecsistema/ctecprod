class AddDeletedAtToClient < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :deleted_at, :timestamp
  end
end
