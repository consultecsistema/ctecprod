# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_30_004841) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clients", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "cnpj"
    t.string "address"
    t.string "number"
    t.string "complement"
    t.string "city"
    t.string "state"
    t.string "cep"
    t.string "contact"
    t.string "phone"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "parent_id"
    t.string "image"
    t.datetime "deleted_at"
    t.index ["parent_id"], name: "index_clients_on_parent_id"
  end

  create_table "executions", id: :serial, force: :cascade do |t|
    t.string "manager"
    t.integer "actable_id"
    t.string "actable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "scheduling_id"
    t.text "observation"
    t.index ["scheduling_id"], name: "index_executions_on_scheduling_id"
  end

  create_table "exhaustions", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inspections", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "hoods_state"
    t.boolean "hoods_clean"
    t.boolean "flame_trap"
    t.boolean "flame_damper"
    t.boolean "flame_system"
    t.boolean "fog"
    t.boolean "risk"
    t.boolean "exhaustion"
    t.boolean "window"
    t.boolean "ducts"
  end

  create_table "pictures", id: :serial, force: :cascade do |t|
    t.integer "execution_id"
    t.string "image"
    t.integer "flag"
    t.index ["execution_id"], name: "index_pictures_on_execution_id"
  end

  create_table "schedulings", id: :serial, force: :cascade do |t|
    t.integer "service"
    t.datetime "date"
    t.integer "client_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "parent_id"
    t.index ["client_id"], name: "index_schedulings_on_client_id"
    t.index ["parent_id"], name: "index_schedulings_on_parent_id"
    t.index ["user_id"], name: "index_schedulings_on_user_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.integer "role"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
  end

  add_foreign_key "executions", "schedulings"
  add_foreign_key "pictures", "executions"
  add_foreign_key "schedulings", "clients"
  add_foreign_key "schedulings", "users"
end
