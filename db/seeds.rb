# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

p 'Creating default admin user.'
user = User.create!(name: "Hugo Amorim", email:"me@hugolyra.com", phone: '(81) 98641-0893', role: User.roles[:administrator])
p 'Sending activation email.'
user.send_activation_email
