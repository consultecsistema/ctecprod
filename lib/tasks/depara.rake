require 'benchmark'
require 'rest_client'
require 'uri'

namespace :depara do
  desc 'TODO'

  task depara_users:  :environment do
    update_users_with_deleted_at
  end

  def update_users_with_deleted_at
    puts 'Start update_users_with_deleted_at...'

    users = User.where.not(deleted_at: nil)
    users.each do |user|
      user.email = 'd3l3t4d0LSM@ctecprod.com'
      user.save
    end

    puts "DONE !"
  end

end
